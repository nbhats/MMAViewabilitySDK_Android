package cn.mmachina.mobile;

/**
 * Created by Amos on 17/12/12.
 * Copyright (c) 2017 MMAChinaSDK. All rights reserved.
 */
public class SignUtils {

    static {
        try {
            System.loadLibrary("MMASignature");

        }catch (Throwable e){

        }
    }

    /**
     * 签名函数
     *
     * @param sdkVersion sdk版本号
     * @param timestamp  事件触发时的时间戳
     * @param originURL  原始监测链接
     * @return 返回签名后的字符串
     */
    public static native String mmaSdkSign(String sdkVersion, String timestamp, String imei,String packagename, String modle,String originURL);


}
