package cn.com.mma.mobile.tracking.demo;


/**
 * MMAChinaSDK Example
 */
public class LandingPageActivity extends BaseActivity {


    @Override
    protected int getContentView() {
        return R.layout.activity_landingpage;
    }

    @Override
    protected String setActionBar() {
        return "LandingPage";
    }

}
